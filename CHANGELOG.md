
# Changelog

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v1.4.1] - 2024-06-28

- Exposed the methods required by uri-resolver [#27424]

## [v1.4.0] - 2024-03-19

**Enhancements**

- Added method update item [#26640]
- Added method deleteDataset [#26793]
- Added method getListExtrasAsHashMap - D4Science model compliant


## [v1.3.0] - 2023-02-06

**Enhancements**

- [#23903] Catalogue Moderation: allow to send a message to the moderators
- [#23692] Changed interface 'approveItem' adding the parameter socialPost

## [v1.2.0] - 2022-08-01

**Enhancements**

- [#23692] Optimized the listing and the paging of catalogue items
- Moved to maven-portal-bom 3.6.4

## [v1.1.0] - 2022-05-18

**New Features**

- [#21643] Integrated with the content-moderator-system facilities
- [#22838] Integrated the collection provided by gCat to read the service configurations
- [#23108] The Moderation facility accessible to Catalogue Editor/Admin in read only mode
- [#23197] Revised the query passed to gCat with the moderation states

## [v1.0.3] - 2022-01-21

**Fixes**
- [#22691] Share Link on private items does not work


## [v1.0.2] - 2021-06-03

**Fixes**
- [#21560] Bug fixing property "visibility" Restricted/Public

## [v1.0.1] - 2021-05-11

**Fixes**
- [#21387] Harmonize the jackson version to v.2.8.11
- Moved to maven-portal-bom.3.6.2


## [v1.0.0] - 2021-02-17

**Bug Fixes**
- [#21259] remove the method that validates the URL

**New Features**
- [#21153] Upgrade the maven-portal-bom to 3.6.1 version
- [#20828] Revisited title size and format
- [#19378] First Release

