package org.gcube.datacatalogue.utillibrary.shared;

/**
 * The current state of this group/user
 * @author Costantino Perciante at ISTI-CNR 
 * (costantino.perciante@isti.cnr.it)
 */
public enum State{
	DELETED, ACTIVE
}