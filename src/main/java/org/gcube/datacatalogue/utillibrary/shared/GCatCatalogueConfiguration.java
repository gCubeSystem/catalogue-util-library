package org.gcube.datacatalogue.utillibrary.shared;

import java.io.Serializable;
import java.util.Set;

public class GCatCatalogueConfiguration implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1250433017161647236L;
	private String context;
	private String defaultOrganization;
	private Set<String> supportedOrganizations;

	private String ckanURL;
	private String solrURL;
	private boolean socialPostEnabled;
	private boolean notificationToUsersEnabled;
	private boolean moderationEnabled;

	public GCatCatalogueConfiguration() {

	}

	public String getContext() {
		return context;
	}

	public String getDefaultOrganization() {
		return defaultOrganization;
	}

	public Set<String> getSupportedOrganizations() {
		return supportedOrganizations;
	}

	public String getCkanURL() {
		return ckanURL;
	}

	public String getSolrURL() {
		return solrURL;
	}

	public boolean isSocialPostEnabled() {
		return socialPostEnabled;
	}

	public boolean isNotificationToUsersEnabled() {
		return notificationToUsersEnabled;
	}

	public boolean isModerationEnabled() {
		return moderationEnabled;
	}

	public void setContext(String context) {
		this.context = context;
	}

	public void setDefaultOrganization(String defaultOrganization) {
		this.defaultOrganization = defaultOrganization;
	}

	public void setSupportedOrganizations(Set<String> supportedOrganizations) {
		this.supportedOrganizations = supportedOrganizations;
	}

	public void setCkanURL(String ckanURL) {
		this.ckanURL = ckanURL;
	}

	public void setSolrURL(String solrURL) {
		this.solrURL = solrURL;
	}

	public void setSocialPostEnabled(boolean socialPostEnabled) {
		this.socialPostEnabled = socialPostEnabled;
	}

	public void setNotificationToUsersEnabled(boolean notificationToUsersEnabled) {
		this.notificationToUsersEnabled = notificationToUsersEnabled;
	}

	public void setModerationEnabled(boolean moderationEnabled) {
		this.moderationEnabled = moderationEnabled;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("GCatCatalogueConfiguration [context=");
		builder.append(context);
		builder.append(", defaultOrganization=");
		builder.append(defaultOrganization);
		builder.append(", supportedOrganizations=");
		builder.append(supportedOrganizations);
		builder.append(", ckanURL=");
		builder.append(ckanURL);
		builder.append(", solrURL=");
		builder.append(solrURL);
		builder.append(", socialPostEnabled=");
		builder.append(socialPostEnabled);
		builder.append(", notificationToUsersEnabled=");
		builder.append(notificationToUsersEnabled);
		builder.append(", moderationEnabled=");
		builder.append(moderationEnabled);
		builder.append("]");
		return builder.toString();
	}

}
