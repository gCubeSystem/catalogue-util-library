package org.gcube.datacatalogue.utillibrary.shared;

/**
 * The Enum ItemStatus.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Feb 17, 2022
 */
public enum ItemStatus {
	PENDING("pending", "Pending"), APPROVED("approved", "Approved"), REJECTED("rejected", "Rejected");
	// Published means that the item is published in the Catalogue, no status is checked, 
	// the "simple" get list of items is called
	//PUBLISHED("published", "Published");

	private String id;
	private String label;

	/**
	 * Instantiates a new item status.
	 *
	 * @param id    the id
	 * @param label the label
	 */
	private ItemStatus(String id, String label) {
		this.id = id;
		this.label = label;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Gets the label.
	 *
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}

}