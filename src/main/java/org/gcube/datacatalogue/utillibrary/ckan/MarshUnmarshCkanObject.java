package org.gcube.datacatalogue.utillibrary.ckan;

import java.io.IOException;

import org.gcube.com.fasterxml.jackson.core.JsonParseException;
import org.gcube.com.fasterxml.jackson.core.JsonProcessingException;
import org.gcube.com.fasterxml.jackson.databind.JsonMappingException;
import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;
import org.gcube.datacatalogue.utillibrary.shared.jackan.model.CkanDataset;
import org.gcube.datacatalogue.utillibrary.shared.jackan.model.CkanGroup;
import org.gcube.datacatalogue.utillibrary.shared.jackan.model.CkanResource;



/**
 * The Class MarshUnmarshCkanObject.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 * Feb 8, 2021
 */
public class MarshUnmarshCkanObject {
	

	/**
	 * The Enum METHOD.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
	 * 
	 * Feb 8, 2021
	 */
	public static enum METHOD {TO_READ, TO_CREATE, TO_UPDATE}
	
	
	/**
	 * Gets the object mapper.
	 *
	 * @param method the method
	 * @param clazz the clazz
	 * @return the object mapper
	 */
	private static ObjectMapper getObjectMapper(METHOD method, Class clazz) {
		ObjectMapper mapper = ExtendCkanClient.getObjectMapper();
		if(method!=null && (method.equals(METHOD.TO_CREATE) || method.equals(METHOD.TO_UPDATE)))
			return ExtendCkanClient.getObjectMapperForPosting(clazz);
		
		return mapper;
	}
	

	/**
	 * To ckan dataset.
	 *
	 * @param jsonDataset the json dataset
	 * @param method the method. Passing TO_CREATE Method the Null fields will be ignored. No otherwise
	 * @return the ckan dataset
	 * @throws JsonParseException the json parse exception
	 * @throws JsonMappingException the json mapping exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public static CkanDataset toCkanDataset(String jsonDataset, METHOD method) throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper om = getObjectMapper(method, CkanDataset.class);
		return om.readValue(jsonDataset, CkanDataset.class);	
	}
	


	/**
	 * To ckan group.
	 *
	 * @param jsonGroup the json group
	 * @return the ckan group
	 * @throws JsonParseException the json parse exception
	 * @throws JsonMappingException the json mapping exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public static CkanGroup toCkanGroup(String jsonGroup) throws JsonParseException, JsonMappingException, IOException {
		return ExtendCkanClient.getObjectMapper().readValue(jsonGroup, CkanGroup.class);	
	}

	
	
	/**
	 * To ckan resource.
	 *
	 * @param jsonResource the json resource
	 * @return the ckan resource
	 * @throws JsonParseException the json parse exception
	 * @throws JsonMappingException the json mapping exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public static CkanResource toCkanResource(String jsonResource) throws JsonParseException, JsonMappingException, IOException {
		return ExtendCkanClient.getObjectMapper().readValue(jsonResource, CkanResource.class);	
	}
	
	
	/**
	 * To json value resource.
	 *
	 * @param resource the resource
	 * @param method the method
	 * @return the string
	 * @throws JsonProcessingException the json processing exception
	 */
	public static String toJsonValueResource(CkanResource resource, METHOD method) throws JsonProcessingException{
		ObjectMapper om = getObjectMapper(method, CkanResource.class);
		return om.writeValueAsString(resource);
	}

	
	/**
	 * To json value dataset.
	 *
	 * @param dataset the dataset
	 * @param method the method
	 * @return the string
	 * @throws JsonProcessingException the json processing exception
	 */
	public static String toJsonValueDataset(CkanDataset dataset, METHOD method) throws JsonProcessingException {
		ObjectMapper om = getObjectMapper(method, CkanDataset.class);
		return om.writeValueAsString(dataset);	
	}
	
	
	/**
	 * To json value group.
	 *
	 * @param group the group
	 * @return the string
	 * @throws JsonProcessingException the json processing exception
	 */
	public static String toJsonValueGroup(CkanGroup group) throws JsonProcessingException {
		return ExtendCkanClient.getObjectMapper().writeValueAsString(group);
	}

}
