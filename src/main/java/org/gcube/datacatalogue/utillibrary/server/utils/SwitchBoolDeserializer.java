package org.gcube.datacatalogue.utillibrary.server.utils;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.gcube.com.fasterxml.jackson.core.JsonParser;
import org.gcube.com.fasterxml.jackson.core.JsonProcessingException;
import org.gcube.com.fasterxml.jackson.databind.DeserializationContext;
import org.gcube.com.fasterxml.jackson.databind.JsonDeserializer;

// TODO: Auto-generated Javadoc
/**
 * The Class SwitchBoolDeserializer.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Jun 3, 2021
 */
public class SwitchBoolDeserializer extends JsonDeserializer<Object> {

	private final static Set<String> trueStateSet = new HashSet<>(Arrays.asList("yes", "true"));

	private final static Set<String> falseStateSet = new HashSet<>(Arrays.asList("no", "false"));

	/**
	 * Deserialize.
	 *
	 * @param jsonParser             the json parser
	 * @param deserializationContext the deserialization context
	 * @return the object
	 * @throws IOException             Signals that an I/O exception has occurred.
	 * @throws JsonProcessingException the json processing exception
	 */
	public Object deserialize(JsonParser jsonParser, DeserializationContext deserializationContext)
			throws IOException, JsonProcessingException {
		String str = jsonParser.getText();
		if (str == null) {
			return null;
		}
		str = str.toLowerCase();
		if (trueStateSet.contains(str)) {
			return true;
		}
		if (falseStateSet.contains(str)) {
			return false;
		}
		return null;
	}

}