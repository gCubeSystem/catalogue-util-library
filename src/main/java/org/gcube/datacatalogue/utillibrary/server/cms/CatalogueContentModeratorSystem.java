package org.gcube.datacatalogue.utillibrary.server.cms;

import java.io.InvalidObjectException;
import java.net.MalformedURLException;
import java.util.List;
import java.util.Map;

import javax.ws.rs.WebApplicationException;
import javax.xml.ws.WebServiceException;

import org.gcube.datacatalogue.utillibrary.shared.ItemStatus;
import org.gcube.datacatalogue.utillibrary.shared.jackan.model.CkanDataset;

/**
 * The Interface CatalogueContentModeratorSystem.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Jan 10, 2022
 */
public interface CatalogueContentModeratorSystem {

	/**
	 * Checks if is moderation enabled.
	 *
	 * @param reloadConfig the reload config
	 * @return true, if is moderation enabled
	 * @throws MalformedURLException
	 */
	boolean isModerationEnabled(boolean reloadConfig) throws MalformedURLException;

	/**
	 * Gets the list items for status.
	 *
	 * @param theStatus    the the status
	 * @param limit        the limit
	 * @param offset       the offset
	 * @param allFields    the all fields. If true returns the all fields of an item
	 * @param filters      add the input filters to query on CKAN
	 * @param sortForField the sort for field
	 * @return the list items for status
	 * @throws WebApplicationException the web application exception
	 * @throws MalformedURLException   the malformed URL exception
	 * @throws InvalidObjectException  the invalid object exception
	 */
	List<CkanDataset> getListItemsForStatus(ItemStatus theStatus, int limit, int offset, Boolean allFields,
			Map<String, String> filters, String sortForField)
			throws WebApplicationException, MalformedURLException, InvalidObjectException;

	/**
	 * Reject item.
	 *
	 * @param itemName          the item name
	 * @param permanentlyDelete the permanently delete
	 * @param reasonMsg         the reason msg
	 * @throws WebServiceException   the web service exception
	 * @throws MalformedURLException the malformed URL exception
	 */
	void rejectItem(String itemName, boolean permanentlyDelete, String reasonMsg)
			throws WebServiceException, MalformedURLException;

	/**
	 * Permanently delete.
	 *
	 * @param itemName the item name
	 * @throws WebServiceException   the web service exception
	 * @throws MalformedURLException the malformed URL exception
	 */
	void permanentlyDelete(String itemName) throws WebServiceException, MalformedURLException;

	/**
	 * Count list items for status.
	 *
	 * @param theStatus the the status
	 * @param filters   add the input filters to query on CKAN
	 * @return the long
	 */
	long countListItemsForStatus(ItemStatus theStatus, Map<String, String> filters);

	/**
	 * Approve item.
	 *
	 * @param itemName         the item name
	 * @param moderatorMessage the moderator message
	 * @param sendSocialPost the send social post. If 'true` sends the social post
	 * @throws WebApplicationException the web application exception
	 * @throws MalformedURLException   the malformed URL exception
	 */
	void approveItem(String itemName, String moderatorMessage, Boolean sendSocialPost) throws WebApplicationException, MalformedURLException;

	/**
	 * Message item.
	 *
	 * @param datasetName      the dataset name
	 * @param moderatorMessage the moderator message
	 * @throws WebServiceException   the web service exception
	 * @throws MalformedURLException the malformed URL exception
	 */
	void messageItem(String datasetName, String moderatorMessage) throws WebServiceException, MalformedURLException;

}
