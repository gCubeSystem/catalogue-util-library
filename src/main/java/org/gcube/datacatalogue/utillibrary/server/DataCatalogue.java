package org.gcube.datacatalogue.utillibrary.server;

import java.util.List;
import java.util.Map;

import org.gcube.datacatalogue.utillibrary.shared.LandingPages;
import org.gcube.datacatalogue.utillibrary.shared.ResourceBean;
import org.gcube.datacatalogue.utillibrary.shared.RolesCkanGroupOrOrg;
import org.gcube.datacatalogue.utillibrary.shared.jackan.model.CkanDataset;
import org.gcube.datacatalogue.utillibrary.shared.jackan.model.CkanGroup;
import org.gcube.datacatalogue.utillibrary.shared.jackan.model.CkanLicense;
import org.gcube.datacatalogue.utillibrary.shared.jackan.model.CkanOrganization;
import org.gcube.datacatalogue.utillibrary.shared.jackan.model.CkanResource;

/**
 * The Interface DataCatalogue.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 *         Feb 12, 2021
 */
public interface DataCatalogue {

	/**
	 * Finds the id associated to the chosen license.
	 *
	 * @param chosenLicense the chosen license
	 * @return the id on success, null otherwise
	 */
	String findLicenseIdByLicenseTitle(String chosenLicense);

	/**
	 * Given the id or the name of the dataset it returns its current url by
	 * contacting the uri resolver. If no uri resolver is available, an url that is
	 * not guaranteed to be long term valid will be generated. Information are not
	 * encrypted.
	 *
	 * @param datasetIdOrName the dataset id or name
	 * @return The url of the dataset on success, null otherwise
	 */
	String getUnencryptedUrlFromDatasetIdOrName(String datasetIdOrName);

	/**
	 * Create a dataset with those information.
	 *
	 * @param nameOrId the name or id
	 * @return the id of the dataset on success, null otherwise
	 */

	/**
	 * Checks if a product with such name already exists. Please remember that the
	 * name is unique.
	 * 
	 * @param nameOrId the name or the id of the dataset to check
	 * @return
	 */
	boolean existProductWithNameOrId(String nameOrId);

	/**
	 * Get the list of licenses' titles.
	 * 
	 * @return the list of licenses' titles
	 */
	List<String> getLicenseTitles();

	/**
	 * Retrieve ckan licenses.
	 *
	 * @return the licenses
	 */
	List<CkanLicense> getLicenses();

	/**
	 * Retrieve the url of the uri resolver for this catalogue instance/scope.
	 *
	 * @return the uri resolver url
	 */
	String getUriResolverUrl();

	/**
	 * Return the manage product property (used in the GRSF[-ADMIN] contexts).
	 *
	 * @return the manage product property
	 */
	boolean isManageProductEnabled();

	/**
	 * Checks if is moderation enabled.
	 *
	 * @param reloadConfig the reload config from service
	 * @return true, if is moderation enabled
	 */
	boolean isModerationEnabled(boolean reloadConfig);

	/**
	 * Return the catalogue portlet for this context(i.e. scope)
	 *
	 * @return the portlet url
	 */
	String getPortletUrl();

	/**
	 * Return the ckan catalogue url in this scope.
	 * 
	 * @return the catalogue url
	 */
	String getCatalogueUrl();

	/**
	 * Returns the list of groups to whom the user belongs (with any role).
	 *
	 * @param username the username
	 * @return a list of groups
	 */
	List<CkanGroup> getGroupsByUser(String username);

	/**
	 * Retrieve a ckan dataset given its id.
	 *
	 * @param datasetIdOrName the dataset id or name
	 * @param username        the username
	 * @return the dataset
	 */
	CkanDataset getDataset(String datasetIdOrName, String username);

	/**
	 * Gets the user role by group.
	 *
	 * @param username the username
	 * @return the user role by group
	 */
	Map<String, Map<CkanGroup, RolesCkanGroupOrOrg>> getUserRoleByGroup(String username);

	/**
	 * The method returns the role the user has in the organizations he/she belongs
	 * to (it uses the db, so it is much faster).
	 *
	 * @param username the username
	 * @return the user role by organization
	 */
	Map<String, Map<CkanOrganization, RolesCkanGroupOrOrg>> getUserRoleByOrganization(String username);

	/**
	 * Retrieve the list of organizations ids.
	 *
	 * @return the organizations ids
	 */
	List<String> getOrganizationsIds();

	/**
	 * Retrieve the list of organizations names.
	 *
	 * @return the organizations names
	 */
	List<String> getOrganizationsNames();

	/**
	 * Retrieve the organization with this name.
	 *
	 * @param name the name
	 * @return the organization by id or name
	 */
	CkanOrganization getOrganizationByIdOrName(String name);

	/**
	 * Returns the list of organizations to whom the user belongs (with any role).
	 *
	 * @param username the username
	 * @return a list of organizations
	 */
	List<CkanOrganization> getOrganizationsByUser(String username);

	/**
	 * Returns the list of organizations' names to whom the user belongs (with any
	 * role).
	 *
	 * @param username the username
	 * @return a list of organizations
	 */
	List<String> getOrganizationsNamesByUser(String username);

	/**
	 * Returns a group given its name.
	 *
	 * @param nameOrId the name or id
	 * @return return a {@link CkanGroup} or null if no group with this name exists
	 */
	CkanGroup getGroupByName(String nameOrId);

	/**
	 * Given the username and the organization name the method retrieves the role of
	 * the user (i.e. his/her 'capacity')
	 *
	 * @param username the username
	 * @param orgName  the org name
	 * @return the capacity of the user into this organization or null
	 */
	String getRoleOfUserInOrganization(String username, String orgName);

	/**
	 * Check if the user is valid by checking if its API_KEY is present into DB.
	 *
	 * @param username the username
	 * @return true, if successful
	 */
	boolean checkValidUser(String username);

	/**
	 * Check if this role is present for this user in the organization. If he/she is
	 * not present we need to add it with the given role.
	 *
	 * @param username                 the username
	 * @param organizationName         the organization name
	 * @param correspondentRoleToCheck the correspondent role to check
	 * @return true if the role can be set, false if it cannot
	 */
	boolean checkRoleIntoOrganization(String username, String organizationName,
			RolesCkanGroupOrOrg correspondentRoleToCheck);

	/**
	 * Check if this role is present for this user in the group. If he/she is not
	 * present we need to add it with the given role.
	 *
	 * @param username                 the username
	 * @param groupName                the group name
	 * @param correspondentRoleToCheck the correspondent role to check
	 * @return true if the role can be set, false if it cannot
	 */
	boolean checkRoleIntoGroup(String username, String groupName, RolesCkanGroupOrOrg correspondentRoleToCheck);

	/**
	 * Returns the main landing pages for this catalogue (i.e. type, orgs, groups
	 * and items pages)
	 *
	 * @return the landing pages
	 * @throws Exception the exception
	 */
	LandingPages getLandingPages() throws Exception;

	/**
	 * Returns a Map with key 'capacity' and as value a list of users with that
	 * capacity into the group groupNameOrTitle.
	 *
	 * @param groupNameOrTitle the group name or title
	 * @return the roles and users group
	 */
	Map<RolesCkanGroupOrOrg, List<String>> getRolesAndUsersGroup(String groupNameOrTitle);

	/**
	 * Get the parent groups of this group.
	 *
	 * @param groupName the group name
	 * @param apiKey    the api key
	 * @return the group parent, if any
	 */
	List<CkanGroup> getParentGroups(String groupName, String apiKey);

	/**
	 * Retrieve the organization with this name.
	 *
	 * @param name the name
	 * @return the organization by name
	 */
	CkanOrganization getOrganizationByName(String name);

	/**
	 * Check if the users of the current context need to be alerted from a post
	 * creation or not. Default is false.
	 * 
	 * @return a boolean value
	 */
	boolean isNotificationToUsersEnabled();

	/**
	 * ***************************************************************************
	 * 
	 * 
	 * WRITE OPERATIONS
	 * 
	 * 
	 * ***************************************************************************.
	 *
	 * @param username             the username
	 * @param title                the title
	 * @param name                 the name
	 * @param organizationName     the organization name
	 * @param author               the author
	 * @param authorMail           the author mail
	 * @param maintainer           the maintainer
	 * @param maintainerMail       the maintainer mail
	 * @param version              the version
	 * @param description          the description
	 * @param licenseId            the license id
	 * @param tags                 the tags
	 * @param customFieldsMultiple the custom fields multiple
	 * @param resources            the resources
	 * @param setPublic            the set public
	 * @param setSearchable        the set searchable
	 * @param socialPost           the social post
	 * @return the string
	 * @throws Exception the exception
	 */

	/**
	 * Create a dataset with those information. The method allows to have multiple
	 * values for the same custom field key. NOTE: unfortunately java doesn't
	 * support overload in java interface methods (that's way I cannot use the same
	 * name for the method)
	 *
	 * @param username             the username
	 * @param title                the title
	 * @param name                 (unique identifier)
	 * @param organizationName     the organization name
	 * @param author               the author
	 * @param authorMail           the author mail
	 * @param maintainer           the maintainer
	 * @param maintainerMail       the maintainer mail
	 * @param version              the version
	 * @param description          the description
	 * @param licenseId            the license id
	 * @param tags                 the tags
	 * @param customFieldsMultiple the custom fields multiple
	 * @param resources            the resources
	 * @param setPublic            (manage visibility: Admin role is needed)
	 * @param setSearchable        set the item as searchable or not
	 * @param socialPost           send the social post or not
	 * @return the id of the dataset on success, null otherwise
	 * @throws Exception the exception
	 */
	String createCkanDatasetMultipleCustomFields(String username, String title, String name, String organizationName,
			String author, String authorMail, String maintainer, String maintainerMail, long version,
			String description, String licenseId, List<String> tags, Map<String, List<String>> customFieldsMultiple,
			List<ResourceBean> resources, boolean setPublic, boolean setSearchable, boolean socialPost)
			throws Exception;

	/**
	 * Update ckan dataset multiple custom fields.
	 *
	 * @param username             the username
	 * @param title                the title
	 * @param name                 the name
	 * @param organizationName     the organization name
	 * @param author               the author
	 * @param authorMail           the author mail
	 * @param maintainer           the maintainer
	 * @param maintainerMail       the maintainer mail
	 * @param version              the version
	 * @param description          the description
	 * @param licenseId            the license id
	 * @param tags                 the tags
	 * @param customFieldsMultiple the custom fields multiple
	 * @param resources            the resources
	 * @param setPublic            the set public
	 * @param setSearchable        the set searchable
	 * @param socialPost           the social post
	 * @return the string
	 * @throws Exception the exception
	 */
	String updateCkanDatasetMultipleCustomFields(String username, String title, String name, String organizationName,
			String author, String authorMail, String maintainer, String maintainerMail, long version,
			String description, String licenseId, List<String> tags, Map<String, List<String>> customFieldsMultiple,
			List<ResourceBean> resources, boolean setPublic, boolean setSearchable, boolean socialPost)
			throws Exception;

	/**
	 * Add a resource described by the bean to the dataset id into
	 * resource.datasetId
	 *
	 * @param resourceBean     the resource bean
	 * @param organizationName the organization name
	 * @param username         the username
	 * @return String the id of the resource on success, null otherwise
	 * @throws Exception the exception
	 */
	String addResourceToDataset(ResourceBean resourceBean, String organizationName, String username) throws Exception;

	/**
	 * Adds the resource.
	 *
	 * @param resourceBean     the resource bean
	 * @param datasetId        the dataset id
	 * @param organizationName the organization name
	 * @param username         the username
	 * @return the ckan resource
	 * @throws Exception the exception
	 */
	CkanResource addResource(ResourceBean resourceBean, String datasetId, String organizationName, String username)
			throws Exception;

	/**
	 * Remove the resource with id resourceId from dataset in which it is.
	 *
	 * @param resourceId the resource id
	 * @param username   the username
	 * @return true on success, false otherwise.
	 * @throws Exception the exception
	 */
	boolean deleteResourceFromDataset(String resourceId, String username) throws Exception;

	/**
	 * Create a CkanGroup.
	 *
	 * @param nameOrId    a unique id for the group
	 * @param title       a title for the group
	 * @param description a description for the group
	 * @return the created CkanGroup on success, null otherwise
	 * @throws Exception the exception
	 */
	CkanGroup createGroup(String nameOrId, String title, String description) throws Exception;

	/**
	 * Sets the searchable field for dataset.
	 *
	 * @param datasetId  the dataset id
	 * @param searchable the searchable
	 * @return true, if successful
	 * @throws Exception the exception
	 */
	boolean setSearchableFieldForDataset(String datasetId, boolean searchable) throws Exception;

	/**
	 * Patch the fields for dataset passed.
	 *
	 * @param datasetId the dataset id
	 * @param mapFields the map fields
	 * @return true, if successful
	 * @throws Exception the exception
	 */
	boolean patchFieldsForDataset(String datasetId, Map<String, String> mapFields) throws Exception;

	/**
	 * Assign dataset to group.
	 *
	 * @param groupNameOrId   the group name or id
	 * @param datasetNameOrId the dataset name or id
	 * @return true, if successful
	 */
	boolean assignDatasetToGroup(String groupNameOrId, String datasetNameOrId);

	/**
	 * Patch a product with product id productId by using the couples in
	 * customFieldsToChange. NOTE: only the specified custom fields will be changed.
	 * If a custom field with a given key already exists, and removeOld is set to
	 * false, the new values are added at the end of the list. Otherwise they are
	 * lost.
	 *
	 * @param productId            the product id
	 * @param username             the username
	 * @param customFieldsToChange the custom fields to change
	 * @param removeOld            the remove old
	 * @return true, if successful
	 */
	boolean patchProductCustomFields(String productId, String username, Map<String, List<String>> customFieldsToChange,
			boolean removeOld);

	/**
	 * Refresh dataset.
	 *
	 * @param datasetName the dataset name
	 * @return the string
	 * @throws Exception the exception
	 */
	String refreshDataset(String datasetName) throws Exception;

	/**
	 * Delete dataset.
	 *
	 * @param datasetName the dataset name
	 * @return true, if successful
	 * @throws Exception the exception
	 */
	boolean deleteDataset(String datasetName) throws Exception;

	/**
	 * Patch a product with product id productId by using the couples in
	 * customFieldsToChange. NOTE: only the specified custom fields will be changed.
	 * If a custom field with a given key already exists, and removeOld is set to
	 * false, the new values are added at the end of the list. Otherwise they are
	 * lost.
	 *
	 * @param username           the username
	 * @param sourceOrganization the source organization
	 * @param currentRole        the current role
	 * @return true, if successful
	 */
//	boolean patchProductCustomFields(String productId, Map<String, List<String>> customFieldsToChange,
//			boolean removeOld);

	/**
	 * ********************************************************************************
	 * 
	 * 
	 * MANAGEMENT OPERATIONS
	 * 
	 * 
	 * ********************************************************************************.
	 *
	 * @param username           the username
	 * @param sourceOrganization the source organization
	 * @param currentRole        the current role
	 */

	/**
	 * Check if the user identified by username has to be registered in other
	 * organizations with some role.
	 * 
	 * @param username
	 * @param sourceOrganization
	 * @param currentRole
	 */
	void assignRolesOtherOrganization(String username, String sourceOrganization, RolesCkanGroupOrOrg currentRole);

}
