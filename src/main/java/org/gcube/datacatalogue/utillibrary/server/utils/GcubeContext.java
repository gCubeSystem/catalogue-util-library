package org.gcube.datacatalogue.utillibrary.server.utils;

/**
 * The Class GcubeContext.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 * Mar 2, 2021
 */
public class GcubeContext {

	String sourceScope;

	String sourceToken;

	String targetScope;

	String targetToken;

	/**
	 * Instantiates a new gcube context.
	 */
	GcubeContext() {

	}

	/**
	 * Instantiates a new gcube context.
	 *
	 * @param sourceScope the source scope
	 * @param sourceToken the source token
	 * @param targetScope the target scope
	 * @param targetToken the target token
	 */
	public GcubeContext(String sourceScope, String sourceToken, String targetScope, String targetToken) {
		super();
		this.sourceScope = sourceScope;
		this.sourceToken = sourceToken;
		this.targetScope = targetScope;
		this.targetToken = targetToken;
	}

	/**
	 * Gets the source scope.
	 *
	 * @return the source scope
	 */
	public String getSourceScope() {
		return sourceScope;
	}

	/**
	 * Sets the source scope.
	 *
	 * @param sourceScope the new source scope
	 */
	public void setSourceScope(String sourceScope) {
		this.sourceScope = sourceScope;
	}

	/**
	 * Gets the source token.
	 *
	 * @return the source token
	 */
	public String getSourceToken() {
		return sourceToken;
	}

	/**
	 * Sets the source token.
	 *
	 * @param sourceToken the new source token
	 */
	public void setSourceToken(String sourceToken) {
		this.sourceToken = sourceToken;
	}

	/**
	 * Gets the target scope.
	 *
	 * @return the target scope
	 */
	public String getTargetScope() {
		return targetScope;
	}

	/**
	 * Sets the target scope.
	 *
	 * @param targetScope the new target scope
	 */
	public void setTargetScope(String targetScope) {
		this.targetScope = targetScope;
	}

	/**
	 * Gets the target token.
	 *
	 * @return the target token
	 */
	public String getTargetToken() {
		return targetToken;
	}

	/**
	 * Sets the target token.
	 *
	 * @param targetToken the new target token
	 */
	public void setTargetToken(String targetToken) {
		this.targetToken = targetToken;
	}

	
}
