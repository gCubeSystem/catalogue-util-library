package org.gcube.datacatalogue.utillibrary.server.utils;

import java.io.IOException;

import org.gcube.com.fasterxml.jackson.core.JsonGenerator;
import org.gcube.com.fasterxml.jackson.databind.JsonSerializer;
import org.gcube.com.fasterxml.jackson.databind.SerializerProvider;

/**
 * The Class SwitchBoolSerializer.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Jun 3, 2021
 */
public class SwitchBoolSerializer extends JsonSerializer<Object> {

	private final static String UNIFIED_TRUE_STRING = "true";

	private final static String UNIFIED_FALSE_STRING = "false";

	/**
	 * Serialize.
	 *
	 * @param o                  the o
	 * @param jsonGenerator      the json generator
	 * @param serializerProvider the serializer provider
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void serialize(Object o, JsonGenerator jsonGenerator, SerializerProvider serializerProvider)
			throws IOException {
		Boolean b = (Boolean) o;
		jsonGenerator.writeString(b ? UNIFIED_TRUE_STRING : UNIFIED_FALSE_STRING);
	}
}
