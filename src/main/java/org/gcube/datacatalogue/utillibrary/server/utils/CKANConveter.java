package org.gcube.datacatalogue.utillibrary.server.utils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.gcube.datacatalogue.utillibrary.ckan.ExtendCkanClient;
import org.gcube.datacatalogue.utillibrary.shared.ResourceBean;
import org.gcube.datacatalogue.utillibrary.shared.jackan.model.CkanDataset;
import org.gcube.datacatalogue.utillibrary.shared.jackan.model.CkanOrganization;
import org.gcube.datacatalogue.utillibrary.shared.jackan.model.CkanPair;
import org.gcube.datacatalogue.utillibrary.shared.jackan.model.CkanResource;
import org.gcube.datacatalogue.utillibrary.shared.jackan.model.CkanTag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.htmlparser.jericho.Renderer;
import net.htmlparser.jericho.Segment;
import net.htmlparser.jericho.Source;

/**
 * The Class CKANConveter.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 * Feb 8, 2021
 */
public class CKANConveter {
	
	
	private static final Logger LOG = LoggerFactory.getLogger(CKANConveter.class);
	
	/**
	 * Convert to ckan dataset. This method creates the body of the dataset
	 *
	 * @param ckanCaller the ckan caller
	 * @param username the username
	 * @param title the title
	 * @param name the name
	 * @param organizationName the organization name or id
	 * @param author the author
	 * @param authorMail the author mail
	 * @param maintainer the maintainer
	 * @param maintainerMail the maintainer mail
	 * @param version the version
	 * @param description the description
	 * @param licenseId the license id
	 * @param tags the tags
	 * @param customFields the custom fields
	 * @param customFieldsMultipleValues the custom fields multiple values
	 * @param resources the resources
	 * @param setPublic the set public
	 * @param setSearchable the set searchable
	 * @return the ckan dataset
	 */
	public static CkanDataset toCkanDataset(ExtendCkanClient ckanCaller, String username, String title, String name, String organizationName, String author,
			String authorMail, String maintainer, String maintainerMail, long version, String description,
			String licenseId, List<String> tags, Map<String, String> customFields,
			Map<String, List<String>> customFieldsMultipleValues, List<ResourceBean> resources, boolean setPublic, boolean setSearchable) {
		
		LOG.info("Called convert data to CkanDataset");
		
		CkanDataset dataset = new CkanDataset();
		
		// in order to avoid errors, the username is always converted
		String ckanUsername = CatalogueUtilMethods.fromUsernameToCKanUsername(username);

		String nameToUse = name;
		//Applying normalize function if and only if the paramenter 'name' is null
		if(nameToUse == null) {
			nameToUse = CatalogueUtilMethods.fromProductTitleToName(title);
		}

		LOG.debug("Name of the dataset is going to be " + nameToUse + ". Title is going to be " + title);

		//TODO the name is required by gCat?
		dataset.setName(nameToUse);
		dataset.setTitle(title);

		//TODO SHOULD BE REVISITED by gCAT?
		if(organizationName!=null) {
			CkanOrganization orgOwner = ckanCaller.getOrganization(organizationName);
			dataset.setOwnerOrg(orgOwner.getId());
		}
		
		dataset.setAuthor(author);
		dataset.setAuthorEmail(authorMail);
		dataset.setMaintainer(maintainer);
		dataset.setMaintainerEmail(maintainerMail);
		dataset.setVersion(String.valueOf(version));
		
		//set is private
		dataset.setPriv(!setPublic);

		// description must be escaped
		if(description != null && !description.isEmpty()){
			Source descriptionEscaped = new Source(description);
			Segment htmlSeg = new Segment(descriptionEscaped, 0, descriptionEscaped.length());
			Renderer htmlRend = new Renderer(htmlSeg);
			dataset.setNotes(htmlRend.toString());
			LOG.debug("Description escaped " + htmlRend.toString());
		}

		dataset.setLicenseId(licenseId);

		// set the tags, if any
		if(tags != null && !tags.isEmpty()){
			List<CkanTag> ckanTags = new ArrayList<CkanTag>(tags.size());
			for (String stringTag : tags) {
				ckanTags.add(new CkanTag(stringTag));
			}
			dataset.setTags(ckanTags);
			dataset.setNumTags(ckanTags.size());
			
		}

		// set the custom fields, if any
		List<CkanPair> extras = new ArrayList<CkanPair>();

		if(customFields != null && !customFields.isEmpty()){

			Iterator<Entry<String, String>> iterator = customFields.entrySet().iterator();

			while (iterator.hasNext()) {
				Map.Entry<String, String> entry = iterator.next();
				extras.add(new CkanPair(entry.getKey(), entry.getValue()));
			}

		}else if(customFieldsMultipleValues != null && !customFieldsMultipleValues.isEmpty()){

			Iterator<Entry<String, List<String>>> iterator = customFieldsMultipleValues.entrySet().iterator();
			while (iterator.hasNext()) {
				Map.Entry<String, List<String>> entry = iterator.next();
				List<String> valuesForEntry = entry.getValue();
				for (String value : valuesForEntry) {
					extras.add(new CkanPair(entry.getKey(), value));
				}
			}
		}

		dataset.setExtras(extras);

		// check if we need to add the resources
		if(resources != null && !resources.isEmpty()){

			LOG.debug("We need to add resources to the dataset");

			try{
				List<CkanResource> resourcesCkan = new ArrayList<CkanResource>();
				for(ResourceBean resource: resources){

					LOG.debug("Going to add resource described by " + resource);
					CkanResource newResource = new CkanResource();
					newResource.setDescription(resource.getDescription());
					newResource.setId(resource.getId());
					newResource.setUrl(resource.getUrl());
					newResource.setName(resource.getName());
					newResource.setMimetype(resource.getMimeType());
					newResource.setFormat(resource.getMimeType());
					newResource.setOwner(ckanUsername);
					resourcesCkan.add(newResource);
				}

				// add to the dataset
				dataset.setResources(resourcesCkan);
				dataset.setNumResources(resourcesCkan.size());

			}catch(Exception e){
				LOG.error("Unable to add those resources to the dataset", e);
			}

		}
		
		return dataset;

	}
	
	
	/**
	 * To ckan resource.
	 *
	 * @param CKAN_CATALOGUE_URL the ckan catalogue url
	 * @param resourceBean the resource bean
	 * @return the ckan resource
	 */
	public static CkanResource toCkanResource(String CKAN_CATALOGUE_URL, ResourceBean resourceBean) {
		
		// in order to avoid errors, the username is always converted
		String ckanUsername = CatalogueUtilMethods.fromUsernameToCKanUsername(resourceBean.getOwner());
		
		CkanResource resource = new CkanResource(CKAN_CATALOGUE_URL, resourceBean.getDatasetId());
		resource.setName(resourceBean.getName());

		// escape description
		Source description = new Source(resourceBean.getDescription());
		Segment htmlSeg = new Segment(description, 0, description.length());
		Renderer htmlRend = new Renderer(htmlSeg);

		resource.setDescription(htmlRend.toString());
		resource.setUrl(resourceBean.getUrl());
		
		//TODO SHOULD BE REVISITED by gCAT?
		resource.setOwner(ckanUsername);
		
		return resource;
		
	}
	

}
