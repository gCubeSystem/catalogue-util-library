package org.gcube.datacatalogue.utillibrary.server;

/**
 * The Class CKANTokenBean.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR Pisa (Italy)
 * Jun 1, 2020
 */
public class CKANTokenBean {

	protected String apiKey;
	protected long timestamp;

	/**
	 * Instantiates a new CKAN token bean.
	 *
	 * @param apiKey the api key
	 * @param timestamp the timestamp
	 */
	public CKANTokenBean(String apiKey, long timestamp) {
		super();
		this.apiKey = apiKey;
		this.timestamp = timestamp;
	}

	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

}
