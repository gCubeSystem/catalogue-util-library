package org.gcube.datacatalogue.utillibrary.server.utils;

import java.net.HttpURLConnection;
import java.net.URL;

import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.common.scope.impl.ScopeBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Some utility methods used within the library.
 * @author Costantino Perciante at ISTI-CNR (costantino.perciante@isti.cnr.it)
 * 
 * @author updated by Francesco Mangiacrapa at ISTI-CNR
 */
public class CatalogueUtilMethods {

	public static final int MAX_SIZE_OF_CKAN_DATASET_NAME = 100;
	private static final Logger logger = LoggerFactory.getLogger(CatalogueUtilMethods.class);

	/**
	 * Maps the scope name to the ckan organization name
	 * Use getCKANOrganization() which uses current scope
	 * @return
	 */
	@Deprecated
	public static String getOrganizationNameFromScope(String scope){
		if(scope == null || scope.isEmpty()) {
			throw new IllegalArgumentException("scope cannot be null");
		}
		ScopeBean scopeBean = new ScopeBean(scope);
		return scopeBean.name().toLowerCase().replace(" ", "_").replace("-", "_");
		
	}

	/**
	 * Get CKAN Organization name by using scope name 
	 * 
	 */
	public static String getCKANOrganization() {
		ScopeBean scopeBean = new ScopeBean(ScopeProvider.instance.get());
		return scopeBean.name().toLowerCase().replace(" ", "_").replace("-", "_");
	}

	/**
	 * Ckan username has _ instead of . (that is, costantino.perciante -> costantino_perciante)
	 * @param owner
	 * @return
	 */
	public static String fromUsernameToCKanUsername(String username){
		if(username == null)
			return null;
		return username.trim().replaceAll("\\.", "_");
	}
	
	/**
	 * Liferay username has . instead of _ (that is, costantino_perciante -> costantino.perciante)
	 * @param owner
	 * @return
	 */
	public static String fromCKanUsernameToUsername(String ckanUsername){
		if(ckanUsername == null)
			return null;

		return ckanUsername.trim().replaceAll("_", ".");
	}

	/**
	 * Generate the catalogue's dataset name from its title
	 * @param title
	 * @return
	 */
	public static String fromProductTitleToName(String title) {
		if(title == null)
			return null;

		String regexTitleNameTransform = "[^A-Za-z0-9_-]";
		title = title.trim().replaceAll(regexTitleNameTransform, "_").replaceAll("_+", "_").toLowerCase();
		if(title.length()>MAX_SIZE_OF_CKAN_DATASET_NAME) {
			return title.substring(0,MAX_SIZE_OF_CKAN_DATASET_NAME-1);
		}
		return title;
			
	}

	/**
	 * Convert a display group name to group id
	 * @param groupName
	 * @return
	 */
	public static String fromGroupTitleToName(String groupName){
		if(groupName == null)
			return null;

		String regexGroupNameTransform = "[^A-Za-z0-9-]";
		String modified = groupName.trim().replaceAll(regexGroupNameTransform, "-").replaceAll("-+", "-").toLowerCase();

		if(modified.startsWith("-"))
			modified = modified.substring(1);
		if(modified.endsWith("-"))
			modified = modified.substring(0, modified.length() -1);

		return modified;

	}

	/**
	 * Utility method to check if a something at this url actually exists
	 * @param urlName
	 * @return
	 */
	public static boolean resourceExists(String urlName){

		if(urlName == null || urlName.isEmpty())
			return false;

		try {
			// replace https
			//String urlToTest = urlName.replace(HTTPS, HTTP);
			String urlToTest = urlName;
			HttpURLConnection.setFollowRedirects(true);
			HttpURLConnection con = (HttpURLConnection) new URL(urlToTest).openConnection();
			con.setRequestMethod("HEAD");
			int responseCode = con.getResponseCode();
			logger.debug("Trying HEAD request to: " + urlToTest);
			logger.debug("HEAD response code is " + responseCode);
			//2×× Success
			boolean exists = (200 <= responseCode) && (responseCode <= 208);
			if(!exists) {
				con = (HttpURLConnection) new URL(urlToTest).openConnection();
				logger.debug("Trying GET request to: " + urlToTest);
				con.setRequestMethod("GET");
				responseCode = con.getResponseCode();
				logger.debug("GET response code is " + responseCode);
				//2×× Success
				exists = (200 <= responseCode) && (responseCode <= 208);
			}
			
			return exists;
		}
		catch (Exception e) {
			logger.error("Exception while checking url", e);
			return false;
		}
	}

	/**
	 * Builds a string made of key + scope
	 * @param key
	 * @param scope
	 * @return
	 */
	public static String concatenateSessionKeyScope(String key, String scope){
		if(key == null || scope == null)
			throw new IllegalArgumentException("Key or scope null");
		return key.concat(scope);
	}
	
	
}
