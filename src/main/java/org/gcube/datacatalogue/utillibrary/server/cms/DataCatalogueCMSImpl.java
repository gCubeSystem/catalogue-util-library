package org.gcube.datacatalogue.utillibrary.server.cms;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.io.InvalidObjectException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ws.rs.WebApplicationException;
import javax.xml.ws.WebServiceException;

import org.gcube.datacatalogue.utillibrary.ckan.MarshUnmarshCkanObject;
import org.gcube.datacatalogue.utillibrary.ckan.MarshUnmarshCkanObject.METHOD;
import org.gcube.datacatalogue.utillibrary.gcat.GCatCaller;
import org.gcube.datacatalogue.utillibrary.shared.ItemStatus;
import org.gcube.datacatalogue.utillibrary.shared.jackan.model.CkanDataset;
import org.gcube.gcat.api.moderation.CMItemStatus;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class DataCatalogueCMSImpl.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Jun 14, 2021
 */
public class DataCatalogueCMSImpl implements CatalogueContentModeratorSystem {

	private static final Logger LOG = LoggerFactory.getLogger(DataCatalogueCMSImpl.class);
	private String runningScope;
	private GCatCaller gCatCaller;

	/**
	 * Instantiates a new data catalogue CMS impl.
	 *
	 * @param runningScope the running scope
	 * @param gCatCaller   the g cat caller
	 */
	public DataCatalogueCMSImpl(String runningScope, GCatCaller gCatCaller) {
		this.runningScope = runningScope;
		this.gCatCaller = gCatCaller;
	}

	/**
	 * Checks if is Moderation is enabled in the working scope.
	 *
	 * @param reloadConfig the reload config
	 * @return true, if is content moderator enabled
	 * @throws MalformedURLException the malformed URL exception
	 */
	@Override
	public boolean isModerationEnabled(boolean reloadConfig) throws MalformedURLException {
		return gCatCaller.getConfiguration(reloadConfig).isModerationEnabled();
	}

	/**
	 * Approve item.
	 *
	 * @param datasetName      the dataset name
	 * @param moderatorMessage the moderator message
	 * @param sendSocialPost the send social post. If 'true` sends the social post
	 * @throws WebApplicationException the web application exception
	 * @throws MalformedURLException   the malformed URL exception
	 */
	@Override
	public void approveItem(String datasetName, String moderatorMessage, Boolean sendSocialPost)
			throws WebApplicationException, MalformedURLException {
		gCatCaller.approveItem(datasetName, moderatorMessage, sendSocialPost);

	}
	
	
	/**
	 * Message item.
	 *
	 * @param datasetName the dataset name
	 * @param moderatorMessage the moderator message
	 * @throws WebServiceException the web service exception
	 * @throws MalformedURLException the malformed URL exception
	 */
	@Override
	public void messageItem(String datasetName,  String moderatorMessage)
			throws WebServiceException, MalformedURLException {
		gCatCaller.messageItem(datasetName, moderatorMessage);

	}

	/**
	 * Reject item.
	 *
	 * @param datasetName       the dataset name
	 * @param permanentlyDelete the permanently delete
	 * @param moderatorMessage  the moderator message
	 * @throws WebServiceException   the web service exception
	 * @throws MalformedURLException the malformed URL exception
	 */
	@Override
	public void rejectItem(String datasetName, boolean permanentlyDelete, String moderatorMessage)
			throws WebServiceException, MalformedURLException {
		gCatCaller.rejectItem(datasetName, permanentlyDelete, moderatorMessage);

	}

	/**
	 * Permanently delete.
	 *
	 * @param datasetName the dataset name
	 * @throws WebServiceException   the web service exception
	 * @throws MalformedURLException the malformed URL exception
	 */
	@Override
	public void permanentlyDelete(String datasetName) throws WebServiceException, MalformedURLException {
		gCatCaller.deleteItem(datasetName, true);

	}

	/**
	 * Gets the running scope.
	 *
	 * @return the running scope
	 */
	public String getRunningScope() {
		return runningScope;
	}

	/**
	 * Gets the list items for status.
	 *
	 * @param theStatus    the the status
	 * @param limit        the limit
	 * @param offset       the offset
	 * @param allFields    the all fields. If true returns the all fields of an item
	 * @param filters      add the input filters to query on CKAN
	 * @param sortForField the sort for field
	 * @return the list items for status
	 * @throws WebApplicationException the web application exception
	 * @throws MalformedURLException   the malformed URL exception
	 * @throws InvalidObjectException  the invalid object exception
	 */
	@Override
	public List<CkanDataset> getListItemsForStatus(ItemStatus theStatus, int limit, int offset, Boolean allFields,
			Map<String, String> filters, String sortForField)
			throws WebApplicationException, MalformedURLException, InvalidObjectException {
		LOG.info("called getListItemsForStatus with [status: " + theStatus + "], [limit: " + limit + "], [offset: "
				+ offset + "], [allFields: " + allFields + "], [filters: " + filters + "]");
		List<CkanDataset> listDataset = null;
		checkNotNull(theStatus);
		JSONArray jsonArray = getSourceArrayOfItemsForStatus(theStatus, limit, offset, allFields, filters, sortForField);

		if (jsonArray != null) {
			int size = jsonArray.size();
			listDataset = new ArrayList<CkanDataset>(size);
			LOG.info("reading and converting " + size + " dataset...");
			for (int i = 0; i < size; i++) {
				String jsonValueDataset = null;
				try {
					CkanDataset toCkanDataset = null;
					if(!allFields) {
						//here the array contains the list of dataset name
						String datasetName = (String) jsonArray.get(i);
						LOG.debug("going to read from gCat the dataset: " + datasetName);
						jsonValueDataset = gCatCaller.getDatasetForName(datasetName);
						LOG.trace("the JSON dataset is: " + jsonValueDataset);
					}else {
						LOG.debug("reading from the array the "+i+"mo json object");
						//here the array contains the list of dataset json fields
						jsonValueDataset = ((JSONObject) jsonArray.get(i)).toString();
						LOG.trace("the JSON dataset is: " + jsonValueDataset);
					}
					toCkanDataset = MarshUnmarshCkanObject.toCkanDataset(jsonValueDataset, METHOD.TO_READ);
					LOG.debug("converted as dataset: " + toCkanDataset);
					listDataset.add(toCkanDataset);
				} catch (IOException e) {
					LOG.warn("Error on reading/converting the dataset: " + jsonValueDataset, e);
				}
			}
		}

		if (listDataset == null) {
			LOG.info("no dataset returned with status: " + theStatus);
			return listDataset;
		}

		LOG.info("returning listDataset with size: " + listDataset.size());
		return listDataset;
	}

	/**
	 * Count list items for status.
	 *
	 * @param theStatus the the status
	 * @param filters   add the input filters to query on CKAN
	 * @return the number of items with the input status
	 */
	@Override
	public long countListItemsForStatus(ItemStatus theStatus, Map<String, String> filters) {
		LOG.info("called countListItemsForStatus with [status: " + theStatus + "]");
		checkNotNull(theStatus);
		int count = 0;
		try {
			CMItemStatus cmStatus = toCMStatus(theStatus);
			count = gCatCaller.countListItemsForCMStatus(cmStatus, filters);
		} catch (Exception e) {
			LOG.error("Error on counting the items for status " + theStatus, e);
			return -1;
		}

		return count;

	}

	/**
	 * Gets the source array of items for status read by gCatClient.
	 *
	 * @param theStatus    the the status
	 * @param limit        the limit
	 * @param offset       the offset
	 * @param allFields    the all fields. If true returns the all fields of an item
	 * @param filters      the filters
	 * @param sortForField the sort for field
	 * @return the source array of items for status
	 * @throws WebApplicationException the web application exception
	 * @throws MalformedURLException   the malformed URL exception
	 * @throws InvalidObjectException  the invalid object exception
	 */
	protected org.json.simple.JSONArray getSourceArrayOfItemsForStatus(ItemStatus theStatus, int limit, int offset,
			Boolean allFields, Map<String, String> filters, String sortForField)
			throws WebApplicationException, MalformedURLException, InvalidObjectException {
		LOG.info("called getSourceArrayOfItemsForStatus with [status: " + theStatus + "], [limit: " + limit
				+ "], [offset: " + offset + "], [allFields: " + allFields + "], [filters: " + filters + "]");
		checkNotNull(theStatus);
		// TODO MUST BE CHANGED FOR THE STATUS
		org.json.simple.JSONArray jsonArray = null;

		/*
		 * String datasetNames = null; if(theStatus.equals(ItemStatus.PUBLISHED)) {
		 * datasetNames = gCatCaller.getListItems(limit, offset); }else { CMItemStatus
		 * cmiStatus = toCMStatus(theStatus); datasetNames =
		 * gCatCaller.getListItemsForCMStatus(cmiStatus, limit, offset); }
		 */

		CMItemStatus cmiStatus = toCMStatus(theStatus);
		String datasetNames = gCatCaller.getListItemsForCMStatus(cmiStatus, limit, offset, allFields, filters,
				sortForField);

		if (datasetNames != null) {
			LOG.trace("for status " + theStatus + " found dataset: " + datasetNames);
			JSONParser parser = new JSONParser();
			try {
				jsonArray = (JSONArray) parser.parse(datasetNames);
			} catch (ParseException e) {
				LOG.error("error occurred reading " + datasetNames + " as JSONArray", e);
				throw new InvalidObjectException(e.getMessage());
			}
		}

		return jsonArray;

	}

	/**
	 * To CM status.
	 *
	 * @param theStatus the the status
	 * @return the CM item status
	 * @throws WebApplicationException the web application exception
	 */
	private CMItemStatus toCMStatus(ItemStatus theStatus) throws WebApplicationException {
		try {
			return CMItemStatus.valueOf(theStatus.name());
		} catch (Exception e) {
			throw new WebApplicationException(
					"No value of " + theStatus.name() + " into enumerator: " + CMItemStatus.values());
		}
	}

}
