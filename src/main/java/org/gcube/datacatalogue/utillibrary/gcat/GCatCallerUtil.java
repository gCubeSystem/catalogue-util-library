package org.gcube.datacatalogue.utillibrary.gcat;

import java.io.InvalidObjectException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.gcube.gcat.api.GCatConstants;
import org.gcube.gcat.api.moderation.CMItemStatus;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class GCatCallerUtil.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Apr 7, 2022
 */
public class GCatCallerUtil {

	private static final Logger LOG = LoggerFactory.getLogger(GCatCallerUtil.class);

	/**
	 * Generic query builder for.
	 *
	 * @param status    the status
	 * @param limit     the limit
	 * @param offset    the offset
	 * @param count     the count. If true returns only the count
	 * @param allFields the all fields. If true returns the all fields of an item
	 * @param filters   the filters
	 * @param sort      the sort
	 * @return the map
	 */
	public static Map<String, String> genericQueryBuilderFor(CMItemStatus status, Integer limit, Integer offset,
			Boolean count, Boolean allFields, Map<String, String> filters, String sort) {
		LOG.info("genericQueryBuilderFor called with [status: " + status + "], [limit: " + limit + "], [offset: "
				+ offset + "], [count: " + count + "], [allFields: " + allFields + "], [filters: " + filters + "]");

		Map<String, String> queryParams = new HashMap<String, String>();

		if (filters != null) {
			StringBuilder queryBuilder = new StringBuilder();

			List<String> keySet = new ArrayList<String>();
			keySet.addAll(filters.keySet());
			String firstKey = keySet.get(0);
			queryBuilder.append(firstKey + ":" + filters.get(firstKey));

			for (int i = 1; i < keySet.size(); i++) {
				String key = keySet.get(i);
				queryBuilder.append(" AND " + key + ":" + filters.get(key));
			}
			queryParams.put("q", queryBuilder.toString());
		}

		if (status != null) {
			queryParams.put(GCatCaller.MODERATOR_ITEM_STATUS_PARAMETER, status.getValue());
		}

		if (limit != null) {
			limit = limit < 0 ? 10 : limit;
			queryParams.put(GCatConstants.LIMIT_QUERY_PARAMETER, String.valueOf(limit));
		}
		if (offset != null) {
			offset = offset < 0 ? 0 : offset;
			queryParams.put(GCatConstants.OFFSET_QUERY_PARAMETER, String.valueOf(offset));
		}

		if (sort != null) {
			queryParams.put("sort", sort);
		} else {
			LOG.info("Adding defautl sort: " + GCatCaller.DEFAULT_SORT_VALUE);
			queryParams.put("sort", GCatCaller.DEFAULT_SORT_VALUE);
		}

		if (count != null) {
			if (count) {
				queryParams.put(GCatConstants.COUNT_QUERY_PARAMETER, String.valueOf(count));
			}
		}

		if (allFields != null) {
			if (allFields) {
				queryParams.put(GCatConstants.ALL_FIELDS_QUERY_PARAMETER, String.valueOf(allFields));
			}
		}

		return queryParams;
	}

	public static List<String> toListString(String gcatListResponse) throws InvalidObjectException {

		List<String> listData = new ArrayList<String>();

		if (gcatListResponse != null) {
			LOG.debug("converting list {}", gcatListResponse);
			JSONParser parser = new JSONParser();
			try {
				JSONArray jsonArray = (JSONArray) parser.parse(gcatListResponse);
				for (Object data : jsonArray) {
					if (data instanceof String) {
						listData.add((String) data);
					}
				}
			} catch (ParseException e) {
				LOG.error("error occurred reading " + gcatListResponse + " as JSONArray", e);
				throw new InvalidObjectException(e.getMessage());
			}
		}

		return listData;
	}

}
