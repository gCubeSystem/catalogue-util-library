/*
 * 
 */
package org.gcube.datacatalogue.utillibrary.gcat;

import java.net.MalformedURLException;
import java.util.Map;

import javax.ws.rs.WebApplicationException;
import javax.xml.ws.WebServiceException;

import org.gcube.datacatalogue.utillibrary.shared.GCatCatalogueConfiguration;
import org.gcube.gcat.api.configuration.CatalogueConfiguration;
import org.gcube.gcat.api.moderation.CMItemStatus;
import org.gcube.gcat.api.moderation.Moderated;
import org.gcube.gcat.client.Configuration;
import org.gcube.gcat.client.Group;
import org.gcube.gcat.client.Item;
import org.gcube.gcat.client.Resource;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class GCatCaller.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR Pisa (Italy) May 29, 2020
 */
public class GCatCaller {

	private String catalogueURL;
	private GCatCatalogueConfiguration catalogueConfiguration;

	private static final Logger LOG = LoggerFactory.getLogger(GCatCaller.class);

	public final static String MODERATOR_ITEM_STATUS_PARAMETER = Moderated.CM_ITEM_STATUS_QUERY_PARAMETER;

	public final static String MODERATOR_ITEM_ANY_STATUS_VALUE = CMItemStatus.ANY.getValue();

	public final static String DEFAULT_SORT_VALUE = "name asc";

	/**
	 * Instantiates a new g cat caller.
	 *
	 * @param catalogueURL the catalogue URL
	 */
	public GCatCaller(String catalogueURL) {
		this.catalogueURL = catalogueURL;
	}

	/**
	 * Gets the catalogue URL.
	 *
	 * @return the catalogue URL
	 */
	public String getCatalogueURL() {
		return catalogueURL;
	}

	/**
	 * Gets the dataset for name.
	 *
	 * @param datasetName the dataset name
	 * @return the jsonValue
	 * @throws WebApplicationException the web application exception
	 * @throws MalformedURLException   the malformed URL exception
	 */
	public String getDatasetForName(String datasetName) throws WebApplicationException, MalformedURLException {
		LOG.debug("getDatasetForName called");
		LOG.info("Get dataset for name '" + datasetName + "' called");
		return new Item().read(datasetName);
	}

	/**
	 * Creates the dataset.
	 *
	 * @param jsonDataset the json dataset
	 * @param socialPost  if true sends the social post
	 * @return the jsonValue
	 * @throws WebApplicationException the web application exception
	 * @throws MalformedURLException   the malformed URL exception
	 */
	public String createDataset(String jsonDataset, boolean socialPost)
			throws WebApplicationException, MalformedURLException {
		LOG.trace("createDataset called");
		LOG.info("Calling create on: " + jsonDataset);
		return new Item().create(jsonDataset, socialPost);
	}

	/**
	 * Update dataset.
	 *
	 * @param datasetName the dataset name
	 * @param jsonDataset the json dataset
	 * @return the string
	 * @throws WebApplicationException the web application exception
	 * @throws MalformedURLException   the malformed URL exception
	 */
	public String updateDataset(String datasetName, String jsonDataset)
			throws WebApplicationException, MalformedURLException {
		LOG.trace("updateDataset called");
		LOG.info("Calling update item with name: " + datasetName + ", on: " + jsonDataset);
		return new Item().update(datasetName, jsonDataset);
	}

	/**
	 * Adds the resource to dataset.
	 *
	 * @param datasetId         the dataset id
	 * @param jsonValueResource the json value resource
	 * @return the string
	 * @throws MalformedURLException the malformed URL exception
	 */
	public String addResourceToDataset(String datasetId, String jsonValueResource) throws MalformedURLException {
		LOG.trace("addResourceToDataset called");
		LOG.info("Calling create resource: " + jsonValueResource + " for dataset id: " + datasetId);
		return new Resource().create(datasetId, jsonValueResource);
	}

	/**
	 * Delete resource.
	 *
	 * @param datasetId  the dataset id
	 * @param resourceId the resource id
	 * @throws MalformedURLException the malformed URL exception
	 */
	public void deleteResource(String datasetId, String resourceId) throws MalformedURLException {
		LOG.trace("deleteResource called");
		LOG.info("Calling delete resource with: " + resourceId + " for dataset id: " + datasetId);
		new Resource().delete(datasetId, resourceId);
	}

	/**
	 * Creates the group.
	 *
	 * @param jsonGroup the json group
	 * @return the string
	 * @throws MalformedURLException the malformed URL exception
	 */
	public String createGroup(String jsonGroup) throws MalformedURLException {
		LOG.trace("createGroup called");
		LOG.info("Calling create group: " + jsonGroup);
		return new Group().create(jsonGroup);
	}

	/**
	 * Patch dataset.
	 *
	 * @param datasetName the dataset name
	 * @param jsonObj     the json obj
	 * @return the string
	 * @throws WebApplicationException the web application exception
	 * @throws MalformedURLException   the malformed URL exception
	 */
	public String patchDataset(String datasetName, JSONObject jsonObj)
			throws WebApplicationException, MalformedURLException {
		LOG.trace("patchDataset called");
		LOG.info("Calling patch dataset with name: " + datasetName);

		return new Item().patch(datasetName, jsonObj.toJSONString());

	}

	/**
	 * Message item. Send a message to Moderators
	 *
	 * @param datasetName      the dataset name
	 * @param moderatorMessage the moderator message
	 * @throws WebApplicationException the web application exception
	 * @throws MalformedURLException   the malformed URL exception
	 */
	public void messageItem(String datasetName, String moderatorMessage)
			throws WebApplicationException, MalformedURLException {
		LOG.trace("messageItem called");
		LOG.info("Calling mesage item with name: " + datasetName + ", and msg: " + moderatorMessage);

		new Item().message(datasetName, moderatorMessage);
		return;
	}

	/**
	 * Approve item.
	 *
	 * @param datasetName      the dataset name
	 * @param moderatorMessage the moderator message
	 * @param sendSocialPost   the send social post. If 'true` sends the social post
	 * @return the string
	 * @throws WebApplicationException the web application exception
	 * @throws MalformedURLException   the malformed URL exception
	 */
	public String approveItem(String datasetName, String moderatorMessage, Boolean sendSocialPost)
			throws WebApplicationException, MalformedURLException {
		LOG.trace("approveItem called");
		LOG.info("Calling approve item with name: " + datasetName + ", sendSocialPost: " + sendSocialPost
				+ ", msg: " + moderatorMessage);

		return new Item().approve(datasetName, moderatorMessage, sendSocialPost);


	}

	/**
	 * Reject item.
	 *
	 * @param datasetName       the dataset name
	 * @param permanentlyDelete the permanently delete
	 * @param moderatorMessage  the moderator message
	 * @return the string
	 * @throws WebServiceException   the web service exception
	 * @throws MalformedURLException the malformed URL exception
	 */
	public String rejectItem(String datasetName, boolean permanentlyDelete, String moderatorMessage)
			throws WebServiceException, MalformedURLException {
		LOG.trace("rejectItem called");
		LOG.info("Calling reject item with name: " + datasetName + ", and msg: " + moderatorMessage);

		String toReturnMsg = new Item().reject(datasetName, moderatorMessage);

		if (permanentlyDelete) {
			deleteItem(datasetName, true);
		}

		return toReturnMsg;
	}

	/**
	 * Delete item.
	 *
	 * @param datasetName the dataset name
	 * @param purge       the purge
	 * @throws WebServiceException   the web service exception
	 * @throws MalformedURLException the malformed URL exception
	 */
	public void deleteItem(String datasetName, boolean purge) throws WebServiceException, MalformedURLException {
		LOG.trace("deleteItem called");
		LOG.info("Calling delete item with name: " + datasetName + ", and purge: " + purge);

		new Item().delete(datasetName, purge);
		return;
	}

	/**
	 * Gets the list items for CM status.
	 *
	 * @param status       the status
	 * @param limit        the limit
	 * @param offset       the offset
	 * @param allFields    the all fields. If true returns the all fields of an item
	 * @param filters      the filters
	 * @param sortForField the sort for field
	 * @return the list items for CM status
	 * @throws WebServiceException   the web service exception
	 * @throws MalformedURLException the malformed URL exception
	 */
	public String getListItemsForCMStatus(CMItemStatus status, int limit, int offset, Boolean allFields,
			Map<String, String> filters, String sortForField) throws WebServiceException, MalformedURLException {
		LOG.trace("getListItemsForCMStatus called");
		LOG.info("called getListItemsForCMStatus called with [status: " + status + "], [limit: " + limit
				+ "], [offset: " + offset + "], [filters: " + filters + "]");

		Map<String, String> queryParams = GCatCallerUtil.genericQueryBuilderFor(status, limit, offset, false, allFields,
				filters, sortForField);

		return getListItemsForQuery(queryParams);
	}

	/**
	 * Count list items for CM status.
	 *
	 * @param status  the status
	 * @param filters the filters
	 * @return the number of items
	 * @throws WebServiceException   the web service exception
	 * @throws MalformedURLException the malformed URL exception
	 */
	public int countListItemsForCMStatus(CMItemStatus status, Map<String, String> filters)
			throws WebServiceException, MalformedURLException {
		LOG.trace("countListItemsForCMStatus called");
		LOG.info("Calling count list items for [status: " + status + "], [filters: " + filters + "]");

		Map<String, String> queryParams = GCatCallerUtil.genericQueryBuilderFor(status, null, null, true, false,
				filters, null);

		String theCount = getListItemsForQuery(queryParams);
		int count = 0;
		try {
			JSONParser parser = new JSONParser();
			JSONObject jsonCount = (JSONObject) parser.parse(theCount);
			Long total = (Long) jsonCount.get("count");
			count = total.intValue();
		} catch (Exception e) {
			LOG.warn("Size retured by gCat is not an integer, returning: " + count, e);
		}
		return count;
	}

	/**
	 * Gets the list items.
	 *
	 * @param limit  the limit
	 * @param offset the offset
	 * @return the list items
	 * @throws WebServiceException   the web service exception
	 * @throws MalformedURLException the malformed URL exception
	 */
	public String getListItems(int limit, int offset) throws WebServiceException, MalformedURLException {
		LOG.trace("getListItems called");
		LOG.info("Calling list items with limit: " + limit + ", offset: " + offset);

		return new Item().list(limit, offset);
	}

	/**
	 * Gets the list items for query.
	 *
	 * @param queryParams the query params
	 * @return the list items for query
	 * @throws WebServiceException   the web service exception
	 * @throws MalformedURLException the malformed URL exception
	 */
	public String getListItemsForQuery(Map<String, String> queryParams)
			throws WebServiceException, MalformedURLException {
		LOG.trace("getListItemsForQuery called");
		LOG.info("Calling list items for query: " + queryParams);

		return new Item().list(queryParams);
	}

	/**
	 * Gets the configuration.
	 *
	 * @param reload the reload
	 * @return the configuration
	 * @throws MalformedURLException the malformed URL exception
	 */
	public GCatCatalogueConfiguration getConfiguration(boolean reload) throws MalformedURLException {
		LOG.trace("getConfiguration called");
		LOG.info("Calling get configuration with reload config: " + reload);

		if (reload || catalogueConfiguration == null) {
			catalogueConfiguration = new GCatCatalogueConfiguration();

			Configuration gCatConfig = new Configuration();
			CatalogueConfiguration gCG = gCatConfig.read();

			catalogueConfiguration.setCkanURL(gCG.getCkanURL());
			catalogueConfiguration.setContext(gCG.getContext());
			catalogueConfiguration.setDefaultOrganization(gCG.getDefaultOrganization());
			catalogueConfiguration.setModerationEnabled(gCG.isModerationEnabled());
			catalogueConfiguration.setSocialPostEnabled(gCG.isSocialPostEnabled());
			catalogueConfiguration.setNotificationToUsersEnabled(gCG.isNotificationToUsersEnabled());
			catalogueConfiguration.setSolrURL(gCG.getSolrURL());
			catalogueConfiguration.setSupportedOrganizations(gCG.getSupportedOrganizations());
		}
		LOG.info("returning gCatConfig: " + catalogueConfiguration);
		return catalogueConfiguration;
	}

	/**
	 * Offset to page number.
	 *
	 * @param limit  the limit
	 * @param offset the offset
	 * @return the int
	 */
	private static int offsetToPageNumber(int limit, int offset) {

		int pageNumber = offset;

		try {
			pageNumber = offset / limit;
		} catch (Exception e) {
			LOG.warn("Page number error: ", e);
		}

		return pageNumber;

	}

}
