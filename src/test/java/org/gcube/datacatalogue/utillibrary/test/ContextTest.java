package org.gcube.datacatalogue.utillibrary.test;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.gcube.common.authorization.utils.manager.SecretManager;
import org.gcube.common.authorization.utils.manager.SecretManagerProvider;
import org.gcube.common.authorization.utils.secret.GCubeSecret;
import org.gcube.common.authorization.utils.secret.Secret;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class ContextTest.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Jul 28, 2022
 */
public class ContextTest {

	private static Logger logger = LoggerFactory.getLogger(ContextTest.class);

	protected static Properties properties;
	protected static final String PROPERTIES_FILENAME = "token.properties";

//	public static final String ROOT = "/gcube";
//	public static final String VO = ROOT + "/devsec";
//	public static final String VRE = VO + "/devVRE";
	
	
	public static final String ROOT = "/d4science.research-infrastructures.eu";
	public static final String VO = ROOT + "/D4OS";
	public static final String VRE = VO + "/Blue-Cloud2026Project";
	
	public static final String scope = VRE;

	static {
		properties = new Properties();
		
		try (InputStream input = ContextTest.class.getClassLoader().getResourceAsStream(PROPERTIES_FILENAME)) {

			if (input == null) {
				String error = "Sorry, unable to find the file: " + PROPERTIES_FILENAME;
				System.out.println(error);
				throw new RuntimeException(error);
			}

			// load a properties file from class path, inside static method
			properties.load(input);

//			// get the property value and print it out
//			System.out.println(prop.getProperty("db.url"));
//			System.out.println(prop.getProperty("db.user"));
//			System.out.println(prop.getProperty("db.password"));

		} catch (IOException ex) {
			ex.printStackTrace();
			throw new RuntimeException(ex);
		}
	}

	/**
	 * Sets the.
	 *
	 * @param secret the secret
	 * @throws Exception the exception
	 */
	public static void set(Secret secret) throws Exception {
		SecretManagerProvider.instance.reset();
		SecretManager secretManager = new SecretManager();
		SecretManagerProvider.instance.set(secretManager);
		secretManager.addSecret(secret);
		secretManager.set();
		String username = secretManager.getUser().getUsername();
		String context = secretManager.getContext();
		logger.debug("Set authorization for user {} in context {}", username, context);
	}

	/**
	 * Sets the context.
	 *
	 * @param token the new context
	 * @throws Exception the exception
	 */
	public static void setContext(String token) throws Exception {
		Secret secret = getSecret(token);
		set(secret);
	}

	/**
	 * Sets the context by name.
	 *
	 * @param fullContextName the new context by name
	 * @throws Exception the exception
	 */
	public static void setContextByName(String fullContextName) throws Exception {
		logger.debug("setContextByName {}", fullContextName);
		Secret secret = getSecretByContextName(fullContextName);
		set(secret);
	}

	/**
	 * Gets the secret.
	 *
	 * @param token the token
	 * @return the secret
	 * @throws Exception the exception
	 */
	private static Secret getSecret(String token) throws Exception {
		GCubeSecret secret = new GCubeSecret(token);
		return secret;
	}

	/**
	 * Gets the secret by context name.
	 *
	 * @param fullContextName the full context name
	 * @return the secret by context name
	 * @throws Exception the exception
	 */
	private static Secret getSecretByContextName(String fullContextName) throws Exception {
		logger.debug("getSecretByContextName {}", fullContextName);
		String token = ContextTest.properties.getProperty(fullContextName);
		logger.debug("token is {}", token);
		return getSecret(token);
	}

	/**
	 * Before class.
	 *
	 * @throws Exception the exception
	 */
	@BeforeClass
	public static void beforeClass() throws Exception {
		logger.debug("beforeClass");
		setContextByName(VRE);
	}

	/**
	 * After class.
	 *
	 * @throws Exception the exception
	 */
	@AfterClass
	public static void afterClass() throws Exception {
		logger.debug("afterClass");
		SecretManagerProvider.instance.reset();
	}

}
