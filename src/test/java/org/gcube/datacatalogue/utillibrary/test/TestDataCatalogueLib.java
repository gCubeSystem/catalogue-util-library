package org.gcube.datacatalogue.utillibrary.test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.datacatalogue.utillibrary.ckan.MarshUnmarshCkanObject;
import org.gcube.datacatalogue.utillibrary.ckan.MarshUnmarshCkanObject.METHOD;
import org.gcube.datacatalogue.utillibrary.server.ApplicationProfileScopePerUrlReader;
import org.gcube.datacatalogue.utillibrary.server.DataCatalogueFactory;
import org.gcube.datacatalogue.utillibrary.server.DataCatalogueImpl;
import org.gcube.datacatalogue.utillibrary.shared.ResourceBean;
import org.gcube.datacatalogue.utillibrary.shared.jackan.model.CkanDataset;
import org.gcube.datacatalogue.utillibrary.shared.jackan.model.CkanGroup;
import org.gcube.datacatalogue.utillibrary.shared.jackan.model.CkanOrganization;
import org.gcube.datacatalogue.utillibrary.shared.jackan.model.CkanUser;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.LoggerFactory;

/**
 * The Class TestDataCatalogueLib.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR Pisa (Italy) Jun 1, 2020
 */
public class TestDataCatalogueLib extends ContextTest{

	private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(TestDataCatalogueLib.class);

	private DataCatalogueFactory factory;
	//private String scope = "/gcube/devsec/devVRE";
	// private String scope = "/gcube";
	// private String scope = "/pred4s/preprod/preVRE";
	// private String scope =
	// "/d4science.research-infrastructures.eu/SoBigData/Catalogue-TerritoriAperti";
	// private String scope =
	// "/d4science.research-infrastructures.eu/gCubeApps/BiodiversityLab";
	// private String testUser = "costantino_perciante";
	private String testUser = "francesco.mangiacrapa";
	//private String authorizationToken = "";
	String subjectId = "aa_father4";
	String objectId = "bb_son4";
	String productName = "adatasetcreatedbycatalogue-util-library-873805063";

	/**
	 * Before.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void before() throws Exception {
		factory = DataCatalogueFactory.getFactory();
	}

	/**
	 * Factory test.
	 *
	 * @throws Exception the exception
	 */
	//@Test
	public void factoryTest() throws Exception {

		DataCatalogueFactory factory = DataCatalogueFactory.getFactory();

		while (true) {
			factory.getUtilsPerScope(scope);
			Thread.sleep(60 * 1000 * 3);
			factory.getUtilsPerScope(scope);
			break;
		}

		for (int i = 0; i < 5; i++) {
			Thread.sleep(1000);
			factory.getUtilsPerScope(scope);
		}

	}

	/**
	 * Gets the scope per url.
	 *
	 * @return the scope per url
	 */
	// @Test
	public void getScopePerUrl() {

		ScopeProvider.instance.set(scope);
		String url = "https://dev2.d4science.org/group/devvre/ckan";
		String scopeToUse = ApplicationProfileScopePerUrlReader.getScopePerUrl(url);
		LOG.debug("Retrieved scope is " + scopeToUse);

		ScopeProvider.instance.reset(); // the following sysout should print null
		String url2 = "https://dev4.d4science.org/group/devvre/ckan";
		String scopeToUse2 = ApplicationProfileScopePerUrlReader.getScopePerUrl(url2);
		LOG.debug("Retrieved scope is " + scopeToUse2);
	}

	/**
	 * Gets the user role by group.
	 *
	 * @return the user role by group
	 * @throws Exception the exception
	 */
	// @Test
	public void getUserRoleByGroup() throws Exception {
		DataCatalogueImpl instance = factory.getUtilsPerScope(scope);
		String username = testUser;
		long init = System.currentTimeMillis();
		instance.getUserRoleByGroup(username);
		long end = System.currentTimeMillis();
		LOG.debug("Time taken " + (end - init));
	}

	/**
	 * Gets the user role by group.
	 *
	 * @return the user role by group
	 * @throws Exception the exception
	 */
	//@Test
	public void getDataset() throws Exception {
		
		String datasetname = "blue-cloud_2026_-_a_federated_european_fair_and_open_research_ecosystem_for_oceans_seas_coastal_and";
		
		DataCatalogueImpl instance = factory.getUtilsPerScope(scope);
		String username = testUser;
		CkanDataset dataset = instance.getDataset(datasetname, username);
		LOG.debug("Got dataset: " + dataset.getName() + ", with id: " + dataset.getId());
		LOG.debug("Got getAuthor: " + dataset.getAuthor());
		LOG.debug("Got getAuthorEmail: " + dataset.getAuthorEmail());
		LOG.debug("Got getMantainer: " + dataset.getMaintainer());
		LOG.debug("Got getMaintainerEmail: " + dataset.getMaintainerEmail());
	}

	/**
	 * Gets the user role by organization.
	 *
	 * @return the user role by organization
	 * @throws Exception the exception
	 */
	// @Test
	public void getUserRoleByOrganization() throws Exception {
		DataCatalogueImpl instance = factory.getUtilsPerScope(scope);
		String username = testUser;
		long init = System.currentTimeMillis();
		instance.getUserRoleByOrganization(username);
		long end = System.currentTimeMillis();
		LOG.debug("Time taken " + (end - init));
	}

	// @Test
	public void getRoleOfUserInOrganization() throws Exception {

		String orgName = "devvre";
		DataCatalogueImpl utils = factory.getUtilsPerScope(scope);
		CkanOrganization org = utils.getOrganizationByIdOrName(orgName);
		String role = utils.getRoleOfUserInOrganization(testUser, org.getName());
		LOG.debug("The user " + testUser + " in the org " + org.getName() + " has the role " + role);
	}

	//@Test
	public void getOrganizationForName() throws Exception {

		String orgName = "devvre";

		DataCatalogueImpl utils = factory.getUtilsPerScope(scope);

		CkanOrganization org = utils.getOrganizationByIdOrName(orgName);

		LOG.debug("The " + CkanOrganization.class.getSimpleName() + " is: " + org.getName());
		LOG.debug("LandingPages of " + CkanOrganization.class.getSimpleName() + " for name " + utils.getLandingPages());
		if (org.getUsers() != null) {

			for (CkanUser user : org.getUsers()) {
				LOG.debug("User: " + user.getName());
			}
		}
	}

	// @Test
	public void getOrganizationsNamesByUser() throws Exception {

		DataCatalogueImpl utils = factory.getUtilsPerScope(scope);

		List<CkanOrganization> listOrgs = utils.getOrganizationsByUser(testUser);

		LOG.debug("User :" + testUser + " found in the Organization/s:");

		for (CkanOrganization ckanOrganization : listOrgs) {
			LOG.debug("Org: " + ckanOrganization.getName());
		}

	}

	// @Test
	public void getGroupsNamesByUser() throws Exception {

		DataCatalogueImpl utils = factory.getUtilsPerScope(scope);

		List<CkanGroup> listGroups = utils.getGroupsByUser(testUser);

		LOG.debug("User :" + testUser + " found in the Group/s:");

		for (CkanGroup ckanGroup : listGroups) {
			LOG.debug("Group: " + ckanGroup.getName());
		}

	}

	// @Test
	public void getLandingPages() throws Exception {

		DataCatalogueImpl utils = factory.getUtilsPerScope(scope);
		LOG.debug("Landing pages " + utils.getLandingPages());

	}

	// @Test
	public void countDatasetForOganization() throws Exception {

		DataCatalogueImpl utils = new DataCatalogueImpl(scope, true);

		List<String> listOrgIds = utils.getOrganizationsIds();

		System.out.println("'ORGANIZATION NAME': 'NUMBER OF PACKAGE'");

		for (String orgID : listOrgIds) {
			CkanOrganization org = utils.getOrganizationByIdOrName(orgID);
			int packageCount = org.getPackageCount();
			// logger.info("ORG: "+org.getName() + " has "+packageCount+ " package/s");
			System.out.println(org.getName() + ": " + packageCount);
		}

	}

	// @Test
	public void createDataset() throws Exception {

		try {

//			ScopeProvider.instance.set(scope);
//			SecurityTokenProvider.instance.set(authorizationToken);

			DataCatalogueImpl instance = factory.getUtilsPerScope(scope);
			String licenseId = instance.getLicenses().get(0).getId();

			Map<String, List<String>> customFieldsMultiple = new HashMap<String, List<String>>();

			for (int i = 0; i < 10; i++) {
				List<String> values = new ArrayList<String>();
				for (int j = 0; j < new Random().nextInt(3); j++) {
					values.add("value-random-" + new Random().nextInt(10));
				}
				customFieldsMultiple.put("key-random-" + new Random().nextInt(10), values);
			}

			customFieldsMultiple.put("empty-key", Arrays.asList(""));

			customFieldsMultiple.put("system:type", Arrays.asList("EmptyProfile"));

			boolean setSearchable = true;
			boolean setPublic = true;
			List<ResourceBean> resources = null;

			String orgName = scope.split("/")[3].toLowerCase();
			// System.out.println("Org name by VRE: "+orgName);
			LOG.debug("Org name by VRE: " + orgName);

			int random = new Random().nextInt();
			String datasetTitle = "a dataset created by catalogue-util-library " + random;
			String datasetName = datasetTitle.replace(" ", "");
			String createdDataset = instance.createCkanDatasetMultipleCustomFields(testUser,
					"a dataset created by catalogue-util-library " + random, datasetName, orgName, testUser, null, null,
					null, 1, "the description", licenseId, Arrays.asList("tag 1", "tag 2", "tag 3"),
					customFieldsMultiple, resources, setPublic, setSearchable, true);

			LOG.info("Created the dataset with id: " + createdDataset);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	// @Test
	public void createGroup() throws Exception {

//		ScopeProvider.instance.set(scope);
//		SecurityTokenProvider.instance.set(authorizationToken);

		DataCatalogueImpl instance = factory.getUtilsPerScope(scope);
		int random = new Random().nextInt();
		String groupTitle = "a grop created by catalogue-util-library " + random;
		String groupName = groupTitle.replace(" ", "");
		CkanGroup ckanGroup = instance.createGroup(groupName, groupTitle, "description");
		LOG.info("Created the group: " + ckanGroup);
	}

	// @Test
	public void addResource() throws Exception {

//		ScopeProvider.instance.set(scope);
//		SecurityTokenProvider.instance.set(authorizationToken);

		DataCatalogueImpl instance = factory.getUtilsPerScope(scope);
		CkanDataset dataset = instance.getDataset(productName, testUser);

		for (int i = 1; i < 3; i++) {
			ResourceBean resourceBean = new ResourceBean("https://data-dev.d4science.net/ie8Y", "resouce " + i,
					"description " + i, null, testUser, dataset.getId(), null);
			instance.addResourceToDataset(resourceBean, testUser, dataset.getOrganization().getName());
		}
	}

	// @Test
	public void deleteResource() throws Exception {

//		ScopeProvider.instance.set(scope);
//		SecurityTokenProvider.instance.set(authorizationToken);

		DataCatalogueImpl instance = factory.getUtilsPerScope(scope);
		String resourceId = "ce7295cf-47db-4faf-901f-4fec2d3fae7a";
		boolean deleted = instance.deleteResourceFromDataset(resourceId, testUser);

		LOG.info("Deleted the resource? " + deleted);
	}

	// @Test
	public void patchFieldsForDataset() throws Exception {

//		ScopeProvider.instance.set(scope);
//		SecurityTokenProvider.instance.set(authorizationToken);

		DataCatalogueImpl instance = factory.getUtilsPerScope(scope);
		Map<String, String> customFieldsToChange = new HashMap<String, String>();
		customFieldsToChange.put("key-random-0", "patched");
		instance.patchFieldsForDataset(productName, customFieldsToChange);
	}

	// @Test
	public void getUrlProduct() throws Exception {

//		ScopeProvider.instance.set(scope);
//		SecurityTokenProvider.instance.set(authorizationToken);

		DataCatalogueImpl instance = factory.getUtilsPerScope(scope);
		String datasetName = productName;
		String url = instance.getUnencryptedUrlFromDatasetIdOrName(datasetName);
		LOG.debug("url is " + url);
	}

	// @Test
	public void marshallingDataset() throws Exception {
		DataCatalogueImpl instance = factory.getUtilsPerScope(scope);
		String username = testUser;
		CkanDataset dataset = instance.getDataset("sarda-sarda", username);
		LOG.debug("Got dataset: " + dataset.getName() + ", with id: " + dataset.getId() + " is private: "
				+ dataset.isPriv());
		String jsonValueDataset = MarshUnmarshCkanObject.toJsonValueDataset(dataset, METHOD.TO_CREATE);
		System.out.println(jsonValueDataset);
		dataset = MarshUnmarshCkanObject.toCkanDataset(jsonValueDataset, METHOD.TO_READ);
		System.out.println(dataset);

	}


}
