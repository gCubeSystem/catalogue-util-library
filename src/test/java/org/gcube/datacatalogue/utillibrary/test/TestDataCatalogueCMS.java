package org.gcube.datacatalogue.utillibrary.test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.datacatalogue.utillibrary.gcat.GCatCaller;
import org.gcube.datacatalogue.utillibrary.server.DataCatalogueFactory;
import org.gcube.datacatalogue.utillibrary.server.DataCatalogueImpl;
import org.gcube.datacatalogue.utillibrary.server.cms.CatalogueContentModeratorSystem;
import org.gcube.datacatalogue.utillibrary.shared.ItemStatus;
import org.gcube.datacatalogue.utillibrary.shared.jackan.model.CkanDataset;
import org.gcube.gcat.client.Item;
import org.slf4j.LoggerFactory;

/**
 * The Class TestDataCatalogueLib.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR Pisa (Italy) Jun 1, 2020
 */
public class TestDataCatalogueCMS extends ContextTest {

	private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(TestDataCatalogueCMS.class);

	private DataCatalogueFactory factory;
	private String testUser = "francesco.mangiacrapa";
	private String scope = "/gcube/devsec/devVRE";
	//private String scope = "/gcube/devsec";
	private String authorizationToken = ""; //devVRE

	/**
	 * Before.
	 *
	 * @throws Exception the exception
	 */
	//@Before
	public void before() throws Exception {
		factory = DataCatalogueFactory.getFactory();
	}

	//@Test
	public void testGCatAvailability() {
		try {
			LOG.info("testGCatAvailability running");

			int offset = 0;
			int limit = 10;
//			ScopeProvider.instance.set(scope);
//			SecurityTokenProvider.instance.set(authorizationToken);
//			LOG.debug("Trying with scope: "+scope);
			String items = new Item().list(limit, offset);
			LOG.debug("List items: " + items);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Factory test.
	 *
	 * @throws Exception the exception
	 */
	//@Test
	public void contentModeratorTest() throws Exception {
//		ScopeProvider.instance.set(scope);
//		SecurityTokenProvider.instance.set(authorizationToken);
		DataCatalogueFactory factory = DataCatalogueFactory.getFactory();
		DataCatalogueImpl dImpl = factory.getUtilsPerScope(scope);
		CatalogueContentModeratorSystem cCMS = dImpl.getCatalogueContentModerator();
		LOG.debug(CatalogueContentModeratorSystem.class.getName() + " instancied correclty");
	}

	//@Test
	public void listItemsForCMStatus() throws Exception {
		try {
			long start = System.currentTimeMillis();
			ItemStatus theStatus = ItemStatus.PENDING;
//			ScopeProvider.instance.set(scope);
//			SecurityTokenProvider.instance.set(authorizationToken);
			DataCatalogueImpl dImpl = factory.getUtilsPerScope(scope);
			CatalogueContentModeratorSystem cCMS = dImpl.getCatalogueContentModerator();
			LOG.debug(CatalogueContentModeratorSystem.class.getName() + " instancied correclty");

			List<String> emailsAddresses = new ArrayList<String>();
			// emailsAddresses.add("luca.frosini@isti.cnr.it");
			emailsAddresses.add("francesco.mangiacrapa@isti.cnr.it");
			// emailsAddresses.add("pagano@cnr.it");

			String theQuery = mockQueryForEmails(emailsAddresses, "OR");

			Map<String, String> filters = new HashMap<String, String>();
			filters.put("author_email", theQuery);

			List<CkanDataset> listItems = cCMS.getListItemsForStatus(theStatus, 10, 0, true, filters,
					GCatCaller.DEFAULT_SORT_VALUE);
			int i = 0;
			for (CkanDataset ckanDataset : listItems) {
				System.out.println(++i + ") item returned: " + ckanDataset);
			}
			long endTime = System.currentTimeMillis()-start;
			System.out.println("Terminated in: "+endTime + "ms");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * Gets the scope per url.
	 *
	 * @return the scope per url
	 * @throws Exception
	 */
	// @Test
	public void countListItemsForStatus() throws Exception {

		try {
			ItemStatus theStatus = ItemStatus.APPROVED;
			ScopeProvider.instance.set(scope);
			SecurityTokenProvider.instance.set(authorizationToken);
			DataCatalogueImpl dImpl = factory.getUtilsPerScope(scope);
			CatalogueContentModeratorSystem cCMS = dImpl.getCatalogueContentModerator();
			LOG.debug(CatalogueContentModeratorSystem.class.getName() + " instancied correclty");

			List<String> emailsAddresses = new ArrayList<String>();
			// emailsAddresses.add("luca.frosini@isti.cnr.it");
			emailsAddresses.add("francesco.mangiacrapa@isti.cnr.it");
			// emailsAddresses.add("pagano@cnr.it");

			String theQuery = mockQueryForEmails(emailsAddresses, "OR");

			Map<String, String> filters = new HashMap<String, String>();
			filters.put("author_email", theQuery);

			long size = cCMS.countListItemsForStatus(theStatus, filters);
			LOG.debug("Size of list of items for status " + theStatus + " is: " + size);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static String mockQueryForEmails(List<String> emailsAddresses, String queryOperator) throws Exception {

		StringBuilder queryMails = new StringBuilder();
		if (queryOperator == null)
			queryOperator = "OR";
		// BUILDING EMAILS QUERY
		int numberOfEmails = emailsAddresses.size();

		String theQuery = "";

		// case 1 email address
		if (numberOfEmails == 1) {
			theQuery = "'" + emailsAddresses.get(0) + "'";
		} else {
			// case N > 1 email addresses
			for (int i = 0; i < emailsAddresses.size() - 1; i++) {
				String email = emailsAddresses.get(i);
				if (i == 0) {
					// opening the query and adding first email address
					queryMails.append("('" + email + "'");
				} else {
					// adding the operator and the email address
					queryMails.append(" " + queryOperator + " '" + email + "'");
				}
			}

			theQuery = queryMails.toString();

			// to be sure that endsWith Operator
			if (!theQuery.endsWith(queryOperator)) {
				theQuery += " " + queryOperator + " ";
			}

			// adding last email address and closing the query
			theQuery += "'" + emailsAddresses.get(numberOfEmails - 1) + "')";
		}

		return theQuery;
	}

}